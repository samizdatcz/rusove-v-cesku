title: "Rusové v Česku: kdo jsou, kde žijí, co studují?"
perex: "Tajné služby každoročně upozorňují, že ruská diaspora může znamenat bezpečnostní riziko. Co lze o českých Rusech zjistit z veřejně dostupných dat?"
authors: ["Jan Boček"]
published: "30. listopadu 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/styles/zpravy_snowfall/public/uploader/rusove_171129-101738_jab.jpg?itok=YY9s-pVl
coverimg_note: "Foto <a href='https://www.flickr.com/photos/jirka_matousek/9101687222/in/photostream/'>Jirka Matousek</a> | Flickr (CC BY 2.0)"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

V Česku žije podle loňských dat Českého statistického úřadu 35 759 Rusů. Přestože jde o zlomek procenta obyvatel, podle výroční zprávy české kontrarozvědky [představují bezpečnostní riziko](http://hlidacipes.org/strcime-jamy-kterou-si-sami-vykopali-ruska-spionaz-cesku-podle-bis/).

Rusové jsou – po Ukrajincích, Slovácích a Vietnamcích – čtvrtou nejsilnější menšinou. Nejsilnější vlna Rusů přicházela do země v letech 2005 až 2009, kdy se jejich počet zdvojnásobil. Pak se ale nárůst zastavil.

<wide>
![Tady bude interaktivní graf](http://samizdat.cz/data/rusove-v-cesku/fotky/rusove.png)
<p>_Tady bude interaktivní graf. Zdroj: ČSÚ_</p>
</wide>

„Po roce 2008 Česko kvůli ekonomické krizi omezilo vydávání povolení k dlouhodobému pobytu i jejich prodlužování,“ vysvětluje Lucie Trlifajová ze Sociologického ústavu Akademie věd. „Nejvíc na to sice doplatili Ukrajinci a Vietnamci, kde se počty udělených víz snížily asi na desetinu, ale částečně se to dotklo i Rusů.“

„Zatímco v roce 2007 české úřady odmítly jen sedm procent ruských žadatelů o dlouhodobé vízum, v následujících letech se podíl odmítnutých pohyboval mezi pětatřiceti a padesáti procenty,“ dodává Trlifajová s odkazem na vlastní [analýzu dat o imigraci](http://migraceonline.cz/cz/e-knihovna/prehled-dat-o-vydavani-dlouhodobych-viz-a-pobytu-v-letech-2007-2011) v období ekonomické krize.

## Praha, Vary, ?

Hlavní baštou Rusů je Praha. Žijí zde asi dvě třetiny, tedy něco přes dvacet tisíc, českých Rusů.

<wide>
![Rusové v Česku](http://samizdat.cz/data/rusove-v-cesku/fotky/kde-cr.jpg)
<p>_Rusové v Česku. Počty Rusů jsou za základní sídelní jednotky o velikosti typicky desítek až stovek, ve velkých městech tisíců lidí. Zdroj: SLDB 2011_</p>
</wide>

Stejně jako u Prahy, ani v Karlových Varech silná ruská diaspora nepřekvapí. Dnes ale Rusy láká ještě třetí město.

„Noví přistěhovalci upřednostňují Teplice,“ komentuje trendy Pavla Holcová z Českého centra pro investigativní žurnalistiku, která ruské prostředí sleduje. „Mají to blízko do Německa, přitom žijí v podobném kulturním prostředí. Trendy jsou také Mariánské Lázně, naopak Karlovy Vary už trochu vyšly z módy.“

Mimo tuto trojici sídel mnoho Rusů nežije; oproti tomu je například ukrajinská komunita mnohem rozptýlenější, častěji žijí v menších obcích.

<wide>
![Rusové v Praze](http://samizdat.cz/data/rusove-v-cesku/fotky/kde-praha.jpg)
<p>_Rusové v Praze. Počty Rusů jsou za základní sídelní jednotky, ty mají v Praze velikost stovek až tisíců lidí. Zdroj: SLDB 2011_</p>
</wide>

V Praze se Rusové soustředí kolem konečné metra C: ve Zličíně nebo Řeporyjích. Vůbec největší podíl jich žije v blocích Zličín-západ (13,3 procenta) a Pod Zbuzany (10,1 procenta). Třetí „ruskou“ oblastí je Starý Bubeneč s necelými sedmi procenty Rusů.

„Za ruskými enklávami v Lužinách, Stodůlkách nebo Butovicích jsou developeři, kteří inzerují své novostavby také v ruštině, někteří dokonce jenom v ruštině,“ vysvětluje Holcová. „Primárně cílí na ruský nebo ukrajinský trh. Řada Rusů je kupuje jako investici nebo kvůli získání pobytového víza, díky kterému se dostanou do celé Evropské unie.“

Příkladem je [realitní agentura Gartal](https://www.gartal.cz/ru) s webem primárně v azbuce. V Česku působí od roku 2005.

## Vyšší vzdělání, pravoslaví, nové byty

Čeští Rusové jsou nadprůměrně vzdělaní a zároveň často věřící, hlásící se k pravoslavné církvi. Oproti většinové populaci je mezi nimi také častější práce v oblasti informačních technologií. Vyplývá to opět ze Sčítání lidí, domů a bytů 2011.

<wide>
![Rusové v Česku: co mají společného?](http://samizdat.cz/data/rusove-v-cesku/fotky/korelace.png)
<p>_Tady bude interaktivní tabulka. Zdroj: SLDB 2011_</p>
</wide>

_Při analýze dat SLDB 2011 jsme počítali sílu vztahu mezi podílem Rusů v základní sídelní jednotce a ostatními proměnnými. Tabulka ukazuje ty s nejsilnější pozitivní nebo negativní korelací._

Silná vazba je také mezi ruskou a ukrajinskou národností. I další data naznačují, že konflikt na východní Ukrajině zdejší imigranty nerozděluje, v Česku se obě ruskojazyčné menšiny často prolínají.

„Kolem ruskojazyčné migrace vyrostl celý ekosystém od právních služeb a pronájmů bytů po kadeřníky nebo veterináře,“ potvrzuje Ondřej Soukup, zahraniční zpravodaj Hospodářských novin, který se zaměřuje na postsovětský prostor.

Další proměnné už ale ukazují na to, že Rusové jsou oproti Ukrajincům o poznání bohatší. Prozrazuje to například vyšší podíl nových bytů mezi Rusy.

„Největší část ruské komunity tvoří střední podnikatelé, převážně z regionů,“ doplňuje Soukup. „Ti nejbohatší se usazují v Londýně, Monaku a podobně, další míří do Vídně nebo Německa. Důvod pro vystěhování je v řadě případů snaha zajistit si ‚záložní letiště‘, dát dětem evropské vzdělání, žít klidnějším a pohodlnějším životem.“

„Rusové napřed přicházeli bohatí, teď sem míří spíš střední třída,“ tvrdí Pavla Holcová z Českého centra pro investigativní žurnalistiku. „U Ukrajinců je to naopak: nejdřív sem mířili za prací ti chudší, až v poslední době se objevují bohatí.“

Úroveň vzdělání v ruské komunitě navíc dále roste: přestože počty Rusů v Česku v posledních letech téměř nerostou, mladých Rusů na českých vysokých školách rychle přibývá.

<wide>
![Tady bude interaktivní graf](http://samizdat.cz/data/rusove-v-cesku/fotky/rusove-vs.png)
<p>_Tady bude interaktivní graf. Zdroj: MŠMT_</p>
</wide>

Ruští studenti si logicky vybírají pražské školy: nejvíc jich studuje na Vysoké škole ekonomické (868 v absolutních číslech a 5,9 procenta ze všech studentů školy), České zemědělské univerzitě (844 a 4,4 procenta) a Univerzitě Karlově (749 a 1,6 procenta). Rusové se také soustředí na některých soukromých školách, jako je Vysoká škola finanční a správní, a.s. (351 a 9,2 procenta) nebo Vysoká škola hotelová (153 a 11,4 procenta).

## XXX

31 tisíc ruských jednatelů

Ruští jednatelé firem jsou starší, typicky mezi 40 a 50, medián 47 let. Ukrajinští majitelé firem mají často těsně po třicítce, medián 42 let. Pro srovnání, medián českých jednatelů je 50 let.

https://www.irozhlas.cz/ekonomika/rusko-cesko-banka-gazprom-erb_1711240655_rez

Vysvětlit, že Rusové obvykle pracují sami na sebe, mají vlastní firmy, ne zaměstnaní

"Český firmy měly najednou mnohem lepší přístup na západoevropskej trh (volnej pohyb zboží a služeb), ve srovnání s nečlenskejma zeměma, takže se víc vyplatilo zakládat si pobočky exportních podniků." Šimon Stiburek

"Když už byli v Česku, bylo snazší prodloužit si víza přes podnikání než zaměstnání, proto spousta založila vlastní firmy (tím se vysvětluje hodně jednatelů mezi nimi). Hodně taky zakládali družstva (tím se vysvětluje korelace s družstevníky u Ukrajinek). Přes s.r.o. pak kupují nemovitosti" - Trlifajová

<left>
	<p></p>
</left>

[hektar](https://cs.wikipedia.org/wiki/Hektar)

<wide>_Pokud vás zajímá, kolik lidí bývá ve dne a v noci ve vaší čtvrti, najděte si ji na mapě, přepněte se na časové řezy, vyberte přepínač hustota osob a potom pohybujte posuvníkem nahoře._</wide>

## Data na prodej